#include <stdio.h>
#include <stdlib.h>

typedef struct arvore {
    int info;
    struct arvore *esq;
    struct arvore *dir;
} Arvore;

Arvore* cria_arv_vazia (void) {
    return NULL;
}

int busca (Arvore *arv, int v) {
    if (arv == NULL) { return 0; }
    else if (v < arv->info) {
        return busca (arv->esq, v);
    }
    else if (v > arv->info) {
        return busca (arv->dir, v);
    }
    else { return 1; }
}

Arvore* insere (Arvore *arv, int v) {
    if (arv == NULL) {
        arv = (Arvore*)malloc(sizeof(Arvore));
        arv->info = v;
        arv->esq = arv->dir = NULL;
    }
    else if (v < arv->info) {
        arv->esq = insere (arv->esq, v);
    }
    else { arv->dir = insere (arv->dir, v); }
    return arv;
}

void ordem(Arvore *arv){
    if(!arv)
        return;
    ordem(arv->esq);
    printf("%d ", arv->info);
    ordem(arv->dir);
}

Arvore *init() {
    Arvore *p;
    p = (Arvore *) malloc(sizeof(Arvore));
    if (!p) {
        printf("Sem alocação!");
        return 0;
    }
    p->esq = NULL;
    p->dir = NULL;
    return p;
}

int eh_espelho(Arvore *arv_a, Arvore *arv_b) {
    if(!arv_a && !arv_b )
        return 1;
    if (!arv_a || !arv_b || arv_a->info != arv_b->info)
        return 0;

    return (eh_espelho((arv_a->esq), (arv_b->dir)) &&
            eh_espelho((arv_a->dir), (arv_b->esq)));
}

Arvore *cria_espelho(Arvore **arv_a) {
    Arvore *nova_arv, *dir, *esq;
    if (!(*arv_a))
        return NULL;

    esq = cria_espelho(&(*arv_a)->esq);
    dir = cria_espelho(&(*arv_a)->dir);

    nova_arv = init();
    nova_arv->info = (*arv_a)->info;
    nova_arv->esq = dir;
    nova_arv->dir = esq;
    return nova_arv;
}

void printA(Arvore *root, int div) {
    if (root == NULL)
        return;
    div += 3;

    printA(root->dir, div);
    printf("\n");
    for (int i = 3; i < div; i++)
        printf(" ");
    printf("%d\n", root->info);

    printA(root->esq, div);
}

void printAD(Arvore *root) {
    printA(root, 0);
}


int main() {
    cria_arv_vazia();
    Arvore *arv, *esp;

    arv = insere(NULL,22);
    arv = insere(arv,41);
    arv = insere(arv,80);
    arv = insere(arv,25);
    arv = insere(arv,37);
    arv = insere(arv,80);
    arv = insere(arv,12);
    arv = insere(arv,38);
    arv = insere(arv,44);
    arv = insere(arv,32);
    printAD(arv);

    esp = cria_espelho(&arv);
    printAD(esp);

    if(eh_espelho(arv, esp)) printf("A árvore é um espelho.\n");

}